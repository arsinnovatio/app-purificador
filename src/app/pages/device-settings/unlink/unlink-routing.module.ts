import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnlinkPage } from './unlink.page';

const routes: Routes = [
  {
    path: '',
    component: UnlinkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UnlinkPageRoutingModule {}
