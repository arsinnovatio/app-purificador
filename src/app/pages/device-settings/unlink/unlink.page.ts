import { Component, OnInit } from '@angular/core';
import { ToastController, Platform, NavController, PopoverController, AlertController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { DeviceService } from "src/app/services/device.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-unlink',
  templateUrl: './unlink.page.html',
  styleUrls: ['./unlink.page.scss'],
})
export class UnlinkPage implements OnInit {
  _t: any = {};
  device: any = {
    id: 12
  }
  settings: any = {};
  updateReady: boolean = true;

  constructor(
    private _translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private nav: NavController,
    public popoverController: PopoverController,
    public deviceService: DeviceService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    if (this.router.getCurrentNavigation().extras.state) {
      this.device = this.router.getCurrentNavigation().extras.state.deviceOBJ;
      this.settings = this.device.settings;
    } else {
      this.route.params.subscribe(async (params) => {
        console.log(params);
        if (params && params.id) {
          var id = params.id;
          this.device = await this.deviceService.getDevice(id);
          this.settings = this.device.settings;
        }
      });
    }
  }

  async confirmUnlink() {
    const alert = await this.alertController.create({
      cssClass: 'alertInput',
      header: this._t.CONFIRMUNLINK_TITLE,
      message: this._t.CONFIRMUNLINK_TEXT,
      buttons: [
        {
          text: this._t.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: this._t.CONFIRM,
          role: 'confirm',
          cssClass: 'primary',
        }
      ]
    });

    await alert.present();
    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);

    if(role == 'confirm'){
      const loading = await this.loadingController.create();
      await loading.present();

      this.deviceService.unlinkDevice(this.device.id).subscribe(async (res: any) => {
        console.log(res);
        await loading.dismiss();

        this.nav.navigateRoot("/home");

      }, async (err) => {
        console.log(err);
        await loading.dismiss();
        await this.toastController.danger("ERROR");
      }) 
    }
  }

}
