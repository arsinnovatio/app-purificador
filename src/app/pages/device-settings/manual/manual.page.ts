import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, Platform, NavController, AlertController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

import { DeviceService } from "src/app/services/device.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-manual',
  templateUrl: './manual.page.html',
  styleUrls: ['./manual.page.scss'],
})
export class ManualPage implements OnInit {
  _t: any = {};
  device: any = {
    id: 12
  }
  settings: any = {};

  constructor(
    private _translate: TranslateService,
    private router: Router,
    private nav: NavController,
    public popoverController: PopoverController,
    public deviceService: DeviceService,
    public alertController: AlertController,
    public toastController: ToastServiceService) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    this.settings = await this.deviceService.getSettings(this.device.id);
  }

}

