import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, Platform, NavController, AlertController, PopoverController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';

import { DeviceService } from "src/app/services/device.service";
import { FeedbackService } from "src/app/services/feedback.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.page.html',
  styleUrls: ['./feedback.page.scss'],
})
export class FeedbackPage implements OnInit {
  _t: any = {};
  device: any = {
    id: 12
  }
  settings: any = {};
  feeds: any = [];
  segment: string = "new";
  feedback: string = "";
  feedbackError: boolean = false;

  constructor(
    private _translate: TranslateService,
    private router: Router,
    private nav: NavController,
    public popoverController: PopoverController,
    public deviceService: DeviceService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public feedbackService: FeedbackService,
    public loadingController: LoadingController) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    this.loadSettings();
    this.loadFeeds();
  }

  async loadSettings() {
    this.settings = await this.deviceService.getSettings(this.device.id);
  }

  async loadFeeds(){
    const loading = await this.loadingController.create();
    await loading.present();

    this.feedbackService.getFeeds().subscribe(async (res: any) => {
      console.log(res.data.feedbacks);
      this.feeds = res.data.feedbacks;
      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.HTTPERROR);
    })
  }

  goToNewFeedback($event){
    this.segment = "new";
  }

  async sendFeedback($event){
    console.log(this.feedback);
    this.feedbackError = false;

    if(this.feedback.length <= 3){
      this.feedbackError = true;
      return;
    }

    const loading = await this.loadingController.create();
    await loading.present();

    this.feedbackService.sendFeedback(this.device.id, {text: this.feedback, response: ''}).subscribe(async (res: any) => {
      console.log(res.data.feedback);
      this.feeds.push(res.data.feedback)
      await loading.dismiss();
      await this.toastController.success(this._t.FEEDBACK_THANK);
      this.segment = "old";
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.FEEDBACK_ERRORSEND);
    })
  }

}
