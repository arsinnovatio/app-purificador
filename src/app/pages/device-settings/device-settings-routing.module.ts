import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceSettingsPage } from './device-settings.page';

const routes: Routes = [
  {
    path: '',
    component: DeviceSettingsPage
  },
  {
    path: 'manual',
    loadChildren: () => import('./manual/manual.module').then( m => m.ManualPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'feedback',
    loadChildren: () => import('./feedback/feedback.module').then( m => m.FeedbackPageModule)
  },
  {
    path: 'unlink',
    loadChildren: () => import('./unlink/unlink.module').then( m => m.UnlinkPageModule)
  },
  {
    path: 'firmware',
    loadChildren: () => import('./firmware/firmware.module').then( m => m.FirmwarePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceSettingsPageRoutingModule {}
