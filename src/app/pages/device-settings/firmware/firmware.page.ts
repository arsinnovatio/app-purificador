import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, Platform, NavController, AlertController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

import { DeviceService } from "src/app/services/device.service";

@Component({
  selector: 'app-firmware',
  templateUrl: './firmware.page.html',
  styleUrls: ['./firmware.page.scss'],
})
export class FirmwarePage implements OnInit {
  _t: any = {};
  device: any = {
    id: 12
  }
  settings: any = {};
  updateReady: boolean = false;

  constructor(
    private _translate: TranslateService,
    private router: Router,
    private nav: NavController,
    public popoverController: PopoverController,
    public deviceService: DeviceService,
    public alertController: AlertController) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    this.settings = await this.deviceService.getSettings(this.device.id);
  }

}
