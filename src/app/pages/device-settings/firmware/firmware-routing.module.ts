import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirmwarePage } from './firmware.page';

const routes: Routes = [
  {
    path: '',
    component: FirmwarePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FirmwarePageRoutingModule {}
