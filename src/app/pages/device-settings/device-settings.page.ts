import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NavController, AlertController, PopoverController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import timezonesJSON from './timezones.json';
import { DeviceService } from "src/app/services/device.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-device-settings',
  templateUrl: './device-settings.page.html',
  styleUrls: ['./device-settings.page.scss'],
})
export class DeviceSettingsPage implements OnInit {
  _t: any = {};
  device: any = {
    id: 12
  }
  settings: any = {};
  timezones = timezonesJSON;
  load = false;

  constructor(private _translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private nav: NavController,
    public popoverController: PopoverController,
    public deviceService: DeviceService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController,
    private detector: ChangeDetectorRef) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    if (this.router.getCurrentNavigation().extras.state) {
      this.device = this.router.getCurrentNavigation().extras.state.deviceOBJ;
      this.settings = this.device.settings;
      this.load = true;
    } else {
      this.route.params.subscribe(async (params) => {
        console.log(params);
        if (params && params.id) {
          var id = params.id;
          console.log(id);

          const loading = await this.loadingController.create();
          await loading.present();

          this.deviceService.getDevice(id).subscribe(async (res: any) => {
            console.log(res.data.device);
            this.device = res.data.device;
            this.settings = this.device.settings;
            await loading.dismiss();
            this.load = true;
          }, async (err) => {
            console.log(err);
            await loading.dismiss();
            await this.toastController.danger(this._t.HTTPERROR);
            this.load = true;
          })

        }
      });
    }
  }

  async setName() {
    const alert = await this.alertController.create({
      header: this._t.NAME,
      message: this._t.CHANGENAME,
      cssClass: 'alertInput',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: this._t.NAME,
          value: this.settings.name
        },
      ],
      buttons: [
        {
          text: this._t.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: this._t.CONFIRM,
          role: 'confirm',
          cssClass: 'primary',
        }
      ]
    });

    await alert.present();

    const { role, data } = await alert.onDidDismiss();

    console.log(data, role)
    if (role == 'confirm' && data) {
      this.deviceService.setSettings(this.device, 'name', data.values.name);
      this.detector.detectChanges();
    }
  }

  async setLocation() {
    const alert = await this.alertController.create({
      header: this._t.LOCATION,
      message: this._t.CHANGELOCATION,
      cssClass: 'alertInput',
      inputs: [
        {
          name: 'location',
          type: 'text',
          placeholder: this._t.LOCATION,
          value: this.settings.location
        },
      ],
      buttons: [
        {
          text: this._t.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: this._t.CONFIRM,
          role: 'confirm',
          cssClass: 'primary',
        }
      ]
    });

    await alert.present();

    const { role, data } = await alert.onDidDismiss();

    console.log(data, role)
    if (role == 'confirm' && data) {
      this.deviceService.setSettings(this.device, 'location', data.values.location)
    }
  }

  changeTime($event) {
    if (this.load) {
      let value = $event.target.value;
      this.deviceService.setSettings(this.device, 'time_format', value)
    }
  }

  changeTimezone($event) {
    if (this.load) {
      let value = $event.target.value;
      console.log(value);
      let zona = this.timezones.find((x) => {
        return x.text == value;
      })

      console.log(zona);
      this.device.settings['time_zone_name'] = zona.text;
      this.deviceService.setSettings(this.device, 'time_zone', zona.offset)
    }
  }

  changeTemp($event) {
    if (this.load) {
      let value = $event.target.value;
      this.deviceService.setSettings(this.device, 'temp_format', value)
    }
  }

  changeDisplay($event) {
    if (this.load) {
      let value = $event.target.value;
      this.deviceService.setSettings(this.device, 'time_display', value)
    }
  }

  goToFirmwareUpdate($event) {
    this.nav.navigateForward('/device/' + this.device.id + '/settings/firmware', { state: { deviceOBJ: this.device } });
  }

  goToUnlink($event) {
    this.nav.navigateForward('/device/' + this.device.id + '/settings/unlink', { state: { deviceOBJ: this.device } });
  }

  goToFeedback($event) {
    this.nav.navigateForward('/device/' + this.device.id + '/settings/feedback', { state: { deviceOBJ: this.device } });
  }

  goToManual($event) {
    this.nav.navigateForward('/device/' + this.device.id + '/settings/manual', { state: { deviceOBJ: this.device } });
  }

  goToFAQ($event) {
    this.nav.navigateForward('/device/' + this.device.id + '/settings/faq', { state: { deviceOBJ: this.device } });
  }
}
