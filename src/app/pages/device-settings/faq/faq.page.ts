import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, Platform, NavController, AlertController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

import { DeviceService } from "src/app/services/device.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  _t: any = {};
  faqs: any = {};
  filteredFaqs: any = {};
  device: any = {
    id: 12
  }
  settings: any = {};
  //One value per question
  visible: any = [false, false]

  constructor(
    private _translate: TranslateService,
    private router: Router,
    private nav: NavController,
    public popoverController: PopoverController,
    public deviceService: DeviceService,
    public alertController: AlertController,
    public toastController: ToastServiceService) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });
    this._translate.get('FAQS').subscribe((res: string) => {
      this.faqs = Object.values(res);
      this.filteredFaqs = this.faqs;
    });
  }

  ngOnInit() {
    this.loadSettings();
  }

  async loadSettings() {
    this.settings = await this.deviceService.getSettings(this.device.id);
  }

  toggle(index) {
    if (this.visible[index])
      this.visible[index] = false;
    else
      this.visible[index] = true
  }

  searchData($event: any) {
    let searchValue = $event.target.value;

    this.filteredFaqs = this.faqs.filter((item) => {
      return item.QUESTION.toLowerCase().includes(searchValue.toLowerCase()) ||
        item.RESPONSE.toLowerCase().includes(searchValue.toLowerCase())
        ;
    });
  }

}

