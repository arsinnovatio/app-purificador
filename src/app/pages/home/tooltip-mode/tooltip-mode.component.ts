import { Component, OnInit, Input } from '@angular/core';
import { PopoverController, AlertController, LoadingController} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { DeviceService } from "src/app/services/device.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-tooltip-mode',
  templateUrl: './tooltip-mode.component.html',
  styleUrls: ['./tooltip-mode.component.scss'],
})
export class TooltipModeComponent implements OnInit {
  @Input() power = false;
  @Input() device: any;
  activeMode: any = {};
  otherModes: any = [];
  _t: any = {};

  constructor(private popoverController: PopoverController,
    private _translate: TranslateService,
    private deviceService: DeviceService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });

    this.activeMode = this.deviceService.getActualMode(this.device);
    this.otherModes = this.deviceService.getModesArray(this.activeMode);

    console.log(this.activeMode)
  }

  async changeMode(mode) {
    console.log("Changin mode to:" + mode);
    const loading = await this.loadingController.create();
    await loading.present();

    this.deviceService.changeMode(this.device, mode).subscribe(async (res: any) => {
      console.log(res);
      this.device.settings = res.data.device.settings;
      this.device.modeObj = this.deviceService.getModeObject(mode);
      this.activeMode = this.deviceService.getActualMode(this.device);
      this.otherModes = this.deviceService.getModesArray(this.activeMode);

      await loading.dismiss();

      await this.popoverController.dismiss(
        {
          'dismissed': false,
          'device': this.device
        });
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    }) 
  }

}
