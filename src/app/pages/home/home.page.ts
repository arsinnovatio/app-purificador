import { Component, OnInit } from '@angular/core';
import { MenuController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { DeviceService } from "src/app/services/device.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  _t: any = {};
  devices: any = [];

  constructor(public menuCtrl: MenuController,
    public deviceService: DeviceService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private _translate: TranslateService) {

    this._translate.get('ERRORS').subscribe((res) => {
      this._t = res;
    });

  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true);
    this.loadDevices(null);
  }

  async ngOnInit() {

  }

  async loadDevices(event) {
    const loading = await this.loadingController.create();
    await loading.present();

    this.deviceService.getDevices().subscribe(async (res: any) => {
      console.log(res);
      this.devices = res.data.devices;
      await loading.dismiss();
      if (event) {
        event.target.complete();
      }
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      const alert = await this.alertController.create({
        header: this._t.HTTPERROR,
        cssClass: "alert-http-error",
        message: Object.values(err.error.error).toString(),
        buttons: ['OK'],
      });

      await alert.present();
      if (event) {
        event.target.complete();
      }
    })
  }

}
