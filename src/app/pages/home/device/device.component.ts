import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonSlides, ToastController, Platform, NavController, AlertController, PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

import { DeviceService } from "src/app/services/device.service";

import { TooltipModeComponent } from "../tooltip-mode/tooltip-mode.component";
import { TooltipPowerComponent } from "../tooltip-power/tooltip-power.component";

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss'],
})
export class DeviceComponent implements OnInit {
  @ViewChild('dataSlider') slider: IonSlides;
  @Input() device;

  _t: any = {};
  slideOptions = { speed: 400, autoplay: { delay: 3500 } }

  constructor(private _translate: TranslateService,
    private router: Router,
    private nav: NavController,
    public popoverController: PopoverController,
    private deviceService: DeviceService) { }

  async ngOnInit() {
    this.device.connected = true;
    this.device.modeObj = this.deviceService.getModeObject(this.device.settings.mode);
  }

  goToDevice() {
    console.log(this.device);
    this.nav.navigateForward('/device/' + this.device.id, {state: {deviceOBJ: this.device}})
  }

  async changeMetter($event: any) {
    $event.stopPropagation();
    if ((await this.slider.length() - 1) === await this.slider.getActiveIndex())
      this.slider.slideTo(0);
    else
      this.slider.slideNext();
  }

  async changeMode($event) {
    $event.stopPropagation();

    const popover = await this.popoverController.create({
      component: TooltipModeComponent,
      cssClass: 'mode-popover',
      event: $event,
      translucent: false,
      mode: "ios",
      componentProps: {
        device: this.device
      }
    });
    await popover.present();

    const { role, data } = await popover.onDidDismiss();

    if(data && data.device){
      this.device = data.device;
      console.log(this.device)
    }
  }

  async changePower($event) {
    $event.stopPropagation();

    const popover = await this.popoverController.create({
      component: TooltipPowerComponent,
      cssClass: 'mode-popover',
      event: $event,
      translucent: false,
      mode: "ios",
      componentProps: {
        power: true
      }
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

}
