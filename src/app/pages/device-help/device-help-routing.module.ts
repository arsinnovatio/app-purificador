import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceHelpPage } from './device-help.page';

const routes: Routes = [
  {
    path: '',
    component: DeviceHelpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceHelpPageRoutingModule {}
