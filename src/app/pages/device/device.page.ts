import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonSlides, ToastController, Platform, NavController, AlertController, PopoverController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import { DeviceService } from "src/app/services/device.service";

import { TooltipHelpComponent } from "./tooltip-help/tooltip-help.component";
import { TooltipModeComponent } from "src/app/pages/home/tooltip-mode/tooltip-mode.component";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-device',
  templateUrl: './device.page.html',
  styleUrls: ['./device.page.scss'],
})
export class DevicePage implements OnInit {
  @ViewChild('dataSlider') slider: IonSlides;
  _t: any = {};
  _tHelp: any = {};
  isDirt = false;
  intervalRefresh = null;

  slideOptions = { speed: 400, autoplay: { delay: 3500 } }

  loading: boolean = true;
  device: any = {
    id: 12
  }

  constructor(private _translate: TranslateService,
    private router: Router,
    private nav: NavController,
    public popoverController: PopoverController,
    private route: ActivatedRoute,
    private deviceService: DeviceService,
    private toastController: ToastServiceService,
    private detector: ChangeDetectorRef) { }

  async ngOnInit() {
    this._translate.get('DEVICE').subscribe((res: string) => {
      this._t = res;
    });

    this._translate.get('TOOLTIPS').subscribe((res: string) => {
      this._tHelp = res;
    });

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.device = this.router.getCurrentNavigation().extras.state.deviceOBJ;
        this.getDevice(this.device.id);
      } else {
        this.route.params.subscribe(async (params) => {
          console.log(params);
          if (params && params.id) {
            var id = params.id;
            this.getDevice(id);

          }
        });
      }
    });

    this.intervalRefresh = setInterval(() => {
      this.getDevice(this.device.id);
    }, 60000)

  }

  ionViewDidLeave(){
    clearInterval(this.intervalRefresh);
  }

  async getDevice(id) {
    this.deviceService.getDevice(id).subscribe(async (res: any) => {
      console.log(res.data.device);
      this.device = res.data.device;
      this.device.modeObj = this.deviceService.getModeObject(this.device.settings.mode);
      this.isDirt = this.deviceService.isDirt(this.device);
      this.loading = false;
      console.log("Completado: ", this.loading)
      this.detector.detectChanges();
    }, async (err) => {
      console.log(err);
      await this.toastController.danger(this._t.HTTPERROR);
      this.loading = false;
    });
  }

  async showHelp($event, item) {
    $event.stopPropagation();

    console.log(this._tHelp[item]);
    let html = this._tHelp[item];

    const popover = await this.popoverController.create({
      component: TooltipHelpComponent,
      cssClass: 'help-popover help-big',
      event: $event,
      translucent: false,
      mode: "ios",
      componentProps: {htmlCode: html}
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async showDisconnected($event) {
    $event.stopPropagation();

    const popover = await this.popoverController.create({
      component: TooltipHelpComponent,
      cssClass: 'help-popover',
      event: $event,
      translucent: false,
      mode: "ios"
    });
    await popover.present();

    const { role } = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async changeMetter($event) {
    $event.stopPropagation();
    if ((await this.slider.length() - 1) === await this.slider.getActiveIndex())
      this.slider.slideTo(0);
    else
      this.slider.slideNext();
  }

  async changeMode($event) {
    $event.stopPropagation();

    const popover = await this.popoverController.create({
      component: TooltipModeComponent,
      cssClass: 'mode-popover',
      event: $event,
      translucent: false,
      mode: "ios",
      componentProps: {
        device: this.device
      }
    });
    await popover.present();

    const { role, data } = await popover.onDidDismiss();

    if (data && data.device) {
      this.device = data.device;
      console.log(this.device)
    }
  }

  goToSettings($event) {
    $event.stopPropagation();

    this.nav.navigateForward('/device/' + this.device.id + '/settings', { state: { deviceOBJ: this.device } })
  }

  goToData($event) {
    $event.stopPropagation();

    this.nav.navigateForward('/device/' + this.device.id + '/data', { state: { deviceOBJ: this.device } })
  }

  transformMeasureToCualification(measure) {
    return this.deviceService.transformMeasureToCualification(this.device, measure);
  }

}
