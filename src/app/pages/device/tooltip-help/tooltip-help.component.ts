import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tooltip-help',
  templateUrl: './tooltip-help.component.html',
  styleUrls: ['./tooltip-help.component.scss'],
})
export class TooltipHelpComponent implements OnInit {
  @Input() htmlCode: string;
  constructor() { }

  ngOnInit() {
    console.log(this.htmlCode);
  }

}
