import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevicePairingHelpPage } from './device-pairing-help.page';

const routes: Routes = [
  {
    path: '',
    component: DevicePairingHelpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevicePairingHelpPageRoutingModule {}
