import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceDataPage } from './device-data.page';

const routes: Routes = [
  {
    path: '',
    component: DeviceDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceDataPageRoutingModule {}
