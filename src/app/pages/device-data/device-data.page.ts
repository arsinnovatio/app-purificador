import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NavController, ModalController, LoadingController, PopoverController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

import { DeviceDataService } from "src/app/services/device-data.service";
import { ToastServiceService } from "src/app/services/toast-service.service";
import { ModalAlertComponent } from './modal-alert/modal-alert.component';

import { Chart, registerables } from "chart.js";
import * as Chartjs from "chart.js";
import * as annotationPlugin from 'chartjs-plugin-annotation';
Chart.register(annotationPlugin);

@Component({
  selector: 'app-device-data',
  templateUrl: './device-data.page.html',
  styleUrls: ['./device-data.page.scss'],
})
export class DeviceDataPage implements OnInit {
  @ViewChild('barCanvas') private barCanvas: ElementRef;
  _t: any = {};
  device: any = {
    id: 12
  }
  segment: string = "statistics";
  barChart: any;
  width: 40

  actualDay = null;
  actualVariable = null;

  dataLabels: any = [];
  dataValues: any = [];
  chartItems: any = [];
  dateItems: any = [];
  listAlert: any = [];

  constructor(private _translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private nav: NavController,
    public popoverController: PopoverController,
    public render: Renderer2,
    public deviceData: DeviceDataService,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public toastController: ToastServiceService) { }

  ngOnInit() {
    this._translate.get('DATAPAGE').subscribe((res: string) => {
      this._t = res;
    });

    this._translate.get('CHARTITEMS').subscribe((res: string) => {
      this.chartItems = Object.values(res);
      this.chartItems[0].active = true;
    });

    this._translate.get('DATAPAGE.DAYSSHORT').subscribe((res: string) => {
      const datesTranslations = Object.values(res);
      console.log(datesTranslations)

      this.dateItems = [];
      for (var i = 0; i < 7; i++) {
        var d = new Date();
        d.setDate(d.getDate() - i);
        d.setHours(1);
        d.setMinutes(0);
        d.setSeconds(0);
        let start = d.getTime();
        d.setHours(23);
        d.setMinutes(59);
        d.setSeconds(59);
        let end = d.getTime();

        this.dateItems.push({ d: (d.getDate()), t: datesTranslations[d.getDay()], start: start, end: end })
      }
      this.dateItems[0].t = this._t.TODAY;
      this.dateItems[0].active = true;
      this.dateItems = this.dateItems.reverse();
      console.log(this.dateItems);
    });
  }

  ionViewDidEnter() {
    this.initData();
  }

  async initData() {
    this.route.params.subscribe(async (params) => {
      console.log(params);
      if (params && params.id) {
        var id = params.id;
        this.device.id = id

        console.log(id);

        this.actualVariable = this.chartItems[0]; //fisrt item of array (temp)
        this.actualDay = this.dateItems[6]; //Last day = today
        this.getData(this.actualVariable.variable, this.actualDay.start, this.actualDay.end, true);
      }
    });
  }

  async getData(variable, start, end, boot) {
    const loading = await this.loadingController.create();
    await loading.present();

    this.deviceData.getData(this.device.id, variable, start, end).subscribe(async (res: any) => {
      console.log(res.data);
      this.device = res.data.device;
      this.dataLabels = Object.keys(res.data.mean);
      this.dataLabels = this.dataLabels.map((e) => {
        return e.split(" ")[1];
      })
      this.dataValues = Object.values(res.data.mean);

      console.log(this.dataLabels);
      console.log(this.dataValues);

      if (boot) {
        this.barChartMethod();
      } else {
        this.updateChartMethod();
      }

      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.HTTPERROR);
    })
  }

  segmentChanged($event) {
    const target = $event.target.value;
    if (target === 'statistics') {
      setTimeout(() => {
        this.barChartMethod();
      }, 200);
    } else {
      this.getAlerts(null);
    }
  }

  getAlerts(event) {
    this.listAlert = this.deviceData.getAlerts();
    console.log(this.listAlert);

    if (event) {
      event.target.complete();
    }
  }

  async changeChartValue(e, index) {
    this.chartItems.forEach(element => {
      element.active = false;
    });
    e.active = true;
    this,this.actualVariable = e;

    this.getData(this.actualVariable.variable, this.actualDay.start, this.actualDay.end, false);
  }

  async changeChartDay(e, index) {
    this.dateItems.forEach(element => {
      element.active = false;
    });
    e.active = true;
    this.actualDay = e;

    this.getData(this.actualVariable.variable, this.actualDay.start, this.actualDay.end, false);
  }

  updateChartMethod() {
    this.barChart.data.labels = this.dataLabels;

    this.barChart.config._config.options.scales.y.min = this.actualVariable.min;
    this.barChart.config._config.options.scales.y.max = this.actualVariable.max;
    
    this.barChart.data.datasets = [{
      label: this.actualVariable.TITLE + '  ' + this.actualVariable.SUBTITLE,
      data: this.dataValues,
      tension: 0.4,
      borderWidth: 10,
      fill: {
        target: 'origin',
        above: 'rgba(62, 86, 104, 1)',   // Area will be red above the origin
      }
    }];

    this.barChart.update();
    console.log(this.barChart);
  }

  backToStatistics($event) {
    this.segment = 'statistics';
  }

  async showAlertModal(alert) {
    const modal = await this.modalController.create({
      component: ModalAlertComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        options: {
          type: alert.type,
          title: alert.title,
          text: alert.text,
          date: alert.date,
        }
      }
    });
    return await modal.present();
  }

  barChartMethod() {
    Chart.register(...registerables);
    // Now we need to supply a Chart element reference with an object that defines the type of chart we want to use, and the type of data we want to display.
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: "line",
      data: {
        labels: this.dataLabels, //['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '24:00'],
        datasets: [{
          label: this.actualVariable.TITLE + '  ' + this.actualVariable.SUBTITLE,
          data: this.dataValues,
          tension: 0.4,
          borderWidth: 10,

          fill: {
            target: 'origin',
            above: 'rgba(62, 86, 104, 1)',   // Area will be red above the origin
          }
        }
      ]
      },
      
      options: {
        plugins: {
          legend: {
            display: false
          }
        },
        layout: {
          padding: {
            left: 0
          }
        },
        scales: {
          x: {
            grid: {
              display: false,
              borderWidth: 1,
              borderColor: 'rgba(62, 86, 104, 1)'
            },
            ticks: {
              color: 'white',
              display: true,
            },

          },
          y: {
            min: this.actualVariable.min,
            max: this.actualVariable.max,
            grid: {
              display: false,
            },
            ticks: {
              display: false,
            }
          }
        }
      },
      plugins: [{
        id: 'scaleBackgroundColor',
        beforeDraw: (chart, args, opts) => {
          const {
            ctx,
            canvas,
            chartArea: {
              left,
              bottom,
              width,
            }
          } = chart;

          ctx.beginPath();
          ctx.rect(left, bottom, width, (canvas.height - bottom));
          ctx.fillStyle = 'rgba(62, 86, 104, 1)'
          ctx.fill();
        }
      }]
    });

  }


}
