import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-alert',
  templateUrl: './modal-alert.component.html',
  styleUrls: ['./modal-alert.component.scss'],
})
export class ModalAlertComponent implements OnInit {
  @Input() options;
  constructor(public modalController: ModalController) { }

  ngOnInit() {

    console.log(this.options);
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true,
      'deleted': false
    });
  }

  delete() {
    this.modalController.dismiss({
      'dismissed': false,
      'deleted': true
    });
  }

}
