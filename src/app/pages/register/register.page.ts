import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router'
import { AuthenticationService } from 'src/app/services/authentication.service'

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  credentialsRegister: FormGroup;
  _t: any = {};

  constructor(private fb: FormBuilder,
    private _translate: TranslateService,
    private router: Router,
    public menuCtrl: MenuController,
    private nav: NavController,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private loadingController: LoadingController) { }

  ngOnInit() {
    this._translate.get('REGISTER').subscribe((res) => {
      this._t = res;
    });

    this._translate.get('ERRORS').subscribe((res) => {
      this._t = {...this._t, ...res }
    });

    this.credentialsRegister = this.fb.group({
      name: [undefined, [Validators.required]],
      surname: [undefined, [Validators.required]],
      email: [undefined, [Validators.required, Validators.email]],
      password: [undefined, [Validators.required, Validators.minLength(8)]],
      password_confirmation: [undefined,
        [
          Validators.required,
          this.matchValues('password'),
        ],
      ],
    });

    this.credentialsRegister.controls.password.valueChanges.subscribe(() => {
      this.credentialsRegister.controls.confirmPassword.updateValueAndValidity();
    });
  }

  ionViewDidEnter() {
    //this.menuCtrl.enable(false);
  }

  async doRegister() {
    const loading = await this.loadingController.create();
    await loading.present();
    
    this.authService.register(this.credentialsRegister.value).subscribe(
      async (res) => {
        await loading.dismiss();        
        this.router.navigateByUrl('/login', { replaceUrl: true });
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
        const alert = await this.alertController.create({
          header: this._t.ERRORREGISTER,
          cssClass: "alert-http-error",
          message: Object.values(res.error.error.detail).toString(),
          buttons: ['OK'],
        });
 
        await alert.present();
      }
    );

  }

  public matchValues(
    matchTo: string // name of the control to match to
  ): (AbstractControl) => ValidationErrors | null {
    return (control): ValidationErrors | null => {
      return !!control.parent &&
        !!control.parent.value &&
        control.value === control.parent.controls[matchTo].value
        ? null
        : { isMatching: false };
    };
  }

  get name() {
    return this.credentialsRegister.get('name');
  }
  
  get surname() {
    return this.credentialsRegister.get('surname');
  }

  get email() {
    return this.credentialsRegister.get('email');
  }
  
  get password() {
    return this.credentialsRegister.get('password');
  }

  get password_confirmation() {
    return this.credentialsRegister.get('password_confirmation');
  }
}
