import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceModesPage } from './device-modes.page';

const routes: Routes = [
  {
    path: '',
    component: DeviceModesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceModesPageRoutingModule {}
