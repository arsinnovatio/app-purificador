import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router'
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private _translate: TranslateService,
    private router: Router,
    public menuCtrl: MenuController,
    private nav: NavController,
    private authService: AuthenticationService) { }

  ngOnInit() {
    
  }

  async ionViewDidEnter(){
    await this.authService.logout();
    this.router.navigateByUrl('/', { replaceUrl: true });
  }

}
