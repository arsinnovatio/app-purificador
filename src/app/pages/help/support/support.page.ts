import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalController, NavController, AlertController, LoadingController } from '@ionic/angular';

import { SupportService } from "src/app/services/support.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

import { ModalNewTicketComponent } from './modal-new-ticket/modal-new-ticket.component';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {
  listTickets: any = [];
  _t: any = {};

  constructor(private _translate: TranslateService,
    public supportService: SupportService,
    public modalController: ModalController,
    public nav: NavController,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) {

    this._translate.get('ERRORS').subscribe((res: string) => {
      this._t = res;
    });
  }

  async ngOnInit() {
    //this.getSupportTickets(null);
  }

  async ionViewDidEnter(){
    this.getSupportTickets(null);
  }

  async getSupportTickets(event) {
    const loading = await this.loadingController.create();
    await loading.present();

    this.supportService.getTickets().subscribe(async (res: any) => {
      this.listTickets = res.data.tickets;
      await loading.dismiss();

      if (event) {
        event.target.complete();
      }
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.SUPPORTERROR);

      if (event) {
        event.target.complete();
      }
    })
  }

  async openModalNewSupport() {
    const modal = await this.modalController.create({
      component: ModalNewTicketComponent,
      cssClass: 'my-custom-class',
      componentProps: {
      }
    });

    modal.onDidDismiss()
      .then((result) => {
        if (result.data != undefined && !result.data.dismissed) {
          this.listTickets.push(result.data.ticket);
        }
      });
    
    return await modal.present();

  }

  showTicket(ticket) {
    this.nav.navigateForward('/help/support/ticket/' + ticket.id, { state: { ticket: ticket } })
  }
}
