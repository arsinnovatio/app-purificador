import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { ModalController, NavController, AlertController, LoadingController } from '@ionic/angular';

import { SupportService } from "src/app/services/support.service";
import { ToastServiceService } from "src/app/services/toast-service.service";


@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.page.html',
  styleUrls: ['./ticket.page.scss'],
})
export class TicketPage implements OnInit {
  _t: any = {};
  ticket: any = {
    id: 1,
    status: 'open',
    date: new Date().getTime(),
    title: 'New support ticket',
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
    responses: [
      {
        date: new Date().getTime(),
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ",
        person: "Andy. D.",
        support: true
      },
    ]
  };
  reply: any = {
    text: ""
  }

  constructor(private router: Router,
    public supportService: SupportService,
    public modalController: ModalController,
    public nav: NavController,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController,
    private _translate: TranslateService) {

    this._translate.get('ERRORS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    if (this.router.getCurrentNavigation().extras.state) {
      this.ticket = this.router.getCurrentNavigation().extras.state.ticket;
    }
  }

  async sendReply() {
    console.log(this.reply);

    const loading = await this.loadingController.create();
    await loading.present();

    this.supportService.updateTicket(this.ticket, this.reply).subscribe(async (res: any) => {
      this.ticket = res.data.ticket;
      await loading.dismiss();
      this.reply.text = "";
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.SUPPORTERROR);
    })
  }

  async delete(){
    const loading = await this.loadingController.create();
    await loading.present();

    this.supportService.deleteTicket(this.ticket).subscribe(async (res: any) => {
      this.nav.back();
      await loading.dismiss();
      this.reply.text = "";
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.SUPPORTERROR);
      this.reply.text = "";
    })
  }

}
