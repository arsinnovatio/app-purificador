import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ModalController, AlertController, LoadingController } from '@ionic/angular';

import { SupportService } from "src/app/services/support.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-modal-new-ticket',
  templateUrl: './modal-new-ticket.component.html',
  styleUrls: ['./modal-new-ticket.component.scss'],
})
export class ModalNewTicketComponent implements OnInit {
  @Input() options;
  deviceList: any = [];
  ticket: any = {
    device_type: "",
    title: "",
    text: "",
  }
  _t: any = {};
  constructor(private _translate: TranslateService,
    public modalController: ModalController,
    public supportService: SupportService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this._translate.get('HELPPAGE').subscribe((res: string) => {
      this._t = res;
    });

    this._translate.get('HELPPAGE.DEVICES').subscribe((res: string) => {
      this.deviceList = Object.values(res);
      console.log(res);
    });
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true,
    });
  }

  async send() {
    const loading = await this.loadingController.create();
    await loading.present();
    
    this.supportService.sendFeedback(this.ticket).subscribe(async (res: any) => {
      console.log(res);
      this.ticket = res.data.ticket;
      await loading.dismiss();

      this.modalController.dismiss({
        'dismissed': false,
        'ticket': this.ticket
      });
      
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger(this._t.SUPPORTERROR);
    });
  }

}
