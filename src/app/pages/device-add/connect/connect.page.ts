import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, Platform, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { BLE } from '@ionic-native/ble/ngx';

import { DeviceConnectService } from "src/app/services/device-connect.service";
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.page.html',
  styleUrls: ['./connect.page.scss'],
})
export class ConnectPage implements OnInit {
  _t: any = {};
  _options: any = {
    ble: false,
    connected: false,
    confirm: false,
    ssid: "",
    password: ""
  }
  step: number = 1; //1=wifi form, 2=loading, 3=success, 4=error,
  device: any = {};
  deviceOBJ: any = {};
  device_id: any = "";
  loading = false;
  timeOutBLEConnect = null;
  intervalHandShake = null;

  constructor(public toastController: ToastController,
    private _translate: TranslateService,
    private ble: BLE,
    private platform: Platform,
    private router: Router,
    private nav: NavController,
    private devConnect: DeviceConnectService,
    private auth: AuthenticationService,
    private detector: ChangeDetectorRef) { }

  ngOnInit() {
    this._translate.get('DEVICE').subscribe((res: string) => {
      this._t = res;
    });

    this.device = this.devConnect.device;
  }

  ionViewDidLeave(){
    console.log("Clean timeout");
    clearTimeout(this.timeOutBLEConnect);
    clearInterval(this.intervalHandShake);
  }

  connect() {

    if (this._options.ssid == "") {
      this.toastError(this._t.ERROR_NOSSID);
      return;
    }
    if (this._options.password == "") {
      this.toastError(this._t.ERROR_PASS);
      return;
    }

    this.changeStep(2);
    console.log("Attepting to connect to: ", this.device.id);

    this.timeOutBLEConnect = setTimeout(() => {
      this.changeStep(4);
      console.log("TIMEOUT waitForConfirm")
    }, 15000);

    if (this.platform.is("hybrid")) {
      this.ble.connect(this.device.id).subscribe((data) => {
        this._options.connected = true;
        this.detector.detectChanges();

        clearTimeout(this.timeOutBLEConnect);

        this.timeOutBLEConnect = setTimeout(() => {
          this.changeStep(4);
          console.log("TIMEOUT waitForConfirm")
        }, 15000);

        this.reciveData();
        this.sendData();

      }, (err) => {
        console.error(err);
        if (err.errorMessage != "Peripheral Disconnected") {
          clearTimeout(this.timeOutBLEConnect);
          this.changeStep(4);
        }
      });
    }
  }

  reciveData() {
    this.ble.startNotification(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400003-B5A3-F393-E0A9-E50E24DCCA9E").subscribe((bleData) => {
      console.log(this.bytesToString(bleData[0]));
      if (this.bytesToString(bleData[0]) == "done") {
        console.log("All data sent");
        this._options.confirm = true;
        this.detector.detectChanges();

        clearTimeout(this.timeOutBLEConnect);

        this.waitForConfirm();
      } else {
        this.device_id = this.bytesToString(bleData[0]);
      }

    }, (err) => {
      console.log(err);
      this.changeStep(4);
    });
  }


  async sendData() {
    if (this.platform.is("hybrid")) {
      try {
        await this.ble.write(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400002-B5A3-F393-E0A9-E50E24DCCA9E", this.stringToBytes(this._options.ssid));
        await this.ble.write(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400002-B5A3-F393-E0A9-E50E24DCCA9E", this.stringToBytes(this._options.password));

        await this.ble.write(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400002-B5A3-F393-E0A9-E50E24DCCA9E", this.stringToBytes(this.auth.token.slice(0, 300)));
        await this.ble.write(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400002-B5A3-F393-E0A9-E50E24DCCA9E", this.stringToBytes(this.auth.token.slice(300, 600)));
        await this.ble.write(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400002-B5A3-F393-E0A9-E50E24DCCA9E", this.stringToBytes(this.auth.token.slice(600, 900)));
        await this.ble.write(this.device.id, "6e400001-b5a3-f393-e0a9-e50e24dcca9e", "6E400002-B5A3-F393-E0A9-E50E24DCCA9E", this.stringToBytes(this.auth.token.slice(900)));

      } catch (error) {
        console.error(error);
        console.log("Error en el try");
      }
    }
  }

  async waitForConfirm() {
    this.intervalHandShake = setInterval(async () => {

      this.devConnect.checkConnection(this.device_id).subscribe(async (res: any) => {
        if (res.data.device != null) {

          this.changeStep(3);
          this.deviceOBJ = res.data.device;

          clearTimeout(this.timeOutBLEConnect);
          clearInterval(this.intervalHandShake);
        }
      }, async (err) => {
        console.log(err);
        clearTimeout(this.timeOutBLEConnect);
        clearInterval(this.intervalHandShake);
        this.changeStep(4);
      })

    }, 5000);

    this.timeOutBLEConnect = setTimeout(async () => {
      console.log("TIMEOUT waitForConfirm")
      clearInterval(this.intervalHandShake);
      this.changeStep(4);
    }, 60000);
  }

  changeStep(step) {
    console.log("changing to step", step);

    this.step = step;

    //For some reason isnt automatic. needed to update view corectlly
    this.detector.detectChanges();
  }

  goToDevice() {
    console.log(this.deviceOBJ);
    this.nav.navigateRoot('/device/' + this.deviceOBJ.id)
  }

  backHome() {
    this.nav.navigateRoot("/home");
  }

  stopConnection() {
    this.changeStep(1);
    this._options.connected = false;
    clearTimeout(this.timeOutBLEConnect);
    clearInterval(this.intervalHandShake);
  }

  tryAgain() {
    this.nav.back();
  }

  goToHelp() {
    this.router.navigateByUrl('/help/faqs')
  }

  async toastError(msg) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'top',
      color: "danger",
      duration: 2000
    });
    toast.present();
  }

  stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
      array[i] = string.charCodeAt(i);
    }
    return array.buffer;
  }

  // ASCII only
  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

}
