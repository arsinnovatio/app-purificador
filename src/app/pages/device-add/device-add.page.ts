import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController, Platform, NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { BLE } from '@ionic-native/ble/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { DeviceConnectService } from "src/app/services/device-connect.service";


@Component({
  selector: 'app-device-add',
  templateUrl: './device-add.page.html',
  styleUrls: ['./device-add.page.scss'],
})
export class DeviceAddPage implements OnInit {
  _t: any = {};
  _options: any = {
    ble: false
  }
  devices = [];
  loading = false;

  constructor(public alertController: AlertController,
    private _translate: TranslateService,
    private ble: BLE,
    private platform: Platform,
    private router: Router,
    private devConnect: DeviceConnectService,
    private androidPermissions: AndroidPermissions) { }

  async ngOnInit() {
    this._translate.get('DEVICE').subscribe((res: string) => {
      this._t = res;
    });

    if (this.platform.ready() && this.platform.is("hybrid")) {
      this._options.ble = this.ble.isEnabled().then(() => { this._options.ble = true }, (err) => { console.error(err); this._options.ble = false });

      if (this.platform.is("android")) {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
          (data) => {
            console.log('Has permission?', data.hasPermission);
            if (data.hasPermission) {
              this.scanDevices(null);
            } else {
              this.askPermision();
            }
          },
          async (err) => {
            await this.askPermision();
          }
        )
      } else
        this.scanDevices(null);

    }

  }

  async askPermision() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: this._t.PERMISSION,
      message: this._t.ASKPERMISION,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    this.scanDevices(null);
  }

  doRefresh(event) {
    this.scanDevices(event);
  }

  async scanDevices(event) {
    this.loading = true;
    this.devices = [];
    if (this.platform.is("hybrid")) {
      this.ble.enable().then(() => {
        this._options.ble = true;
        this.ble.startScan([]).subscribe((response) => {
          if (response.name == "Infinion-Ventilacion") {
            this.devices.push(response);
            this.loading = false;
          }
        }, (err) => {
          console.error(err);
        });

        setTimeout(() => {
          this.ble.stopScan();
          this.loading = false;
          if (event) {
            event.target.complete();
          }
        }, 15000);

      });


    } else {
      this.fakeScan(event);
    }
  }

  fakeScan(event) {
    setTimeout(async () => {
      this.devices.push({ id: "4D:D4:63:06:D6:93", rssi: -62 });
      this.devices.push({ id: "4D:D4:63:06:D6:93", name: "Amafit bip watcher molon", rssi: -62 });
      this.devices.push({ id: "4D:D4:63:06:D6:93", name: "Dispositivo clima", rssi: -62 });
      this.devices.push({ id: "4D:D4:63:06:D6:93", rssi: -62 });
      this.devices.push({ id: "4D:D4:63:06:D6:93", rssi: -62 });

      this.loading = false;

      if (event) {
        event.target.complete();
      }
    }, 4000);
  }

  addDevice(device) {
    this.devConnect.device = device;
    this.router.navigateByUrl('/device-add/device/' + device.id)
  }

}
