import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-recover',
  templateUrl: './recover.page.html',
  styleUrls: ['./recover.page.scss'],
})
export class RecoverPage implements OnInit {
  _t: any = {};
  step: number = 1;
  private sub: any;
  formData = {
    email: "",
    token: "",
    password: "",
    repeatPassword: ""
  }

  constructor(private _translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    public menuCtrl: MenuController) { }

  ngOnInit() {
    this._translate.get('RECOVER').subscribe((res: string) => {
      this._t = res;
    });
  }

  ionViewDidEnter() {   
    this.menuCtrl.enable(false);

    this.sub = this.route.params.subscribe(params => {
      let token = params['token'];

      if (token) {
        this.formData.token = token;
        this.step = 2;

        console.log("tenemos code", token)
      }
    })
  }
  

  doRecover() {
    this.step = 2;
  }

  goNext() {
    this.step = 2;
  }

  doVerify() {
    this.step = 3;
  }

  doChangePassword() {
    this.router.navigateByUrl('/login')
  }

}
