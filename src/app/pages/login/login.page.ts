import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentials: FormGroup;
  _t: any = {};

  constructor(
    private fb: FormBuilder,
    private _translate: TranslateService,
    private router: Router,
    public menuCtrl: MenuController,
    private nav: NavController,
    private authService: AuthenticationService,
    private alertController: AlertController,
    private loadingController: LoadingController,
    public toastController: ToastServiceService) { }

  ngOnInit() {
    this._translate.get('LOGIN').subscribe((res: string) => {
      this._t = res;
    });

    this._translate.get('ERRORS').subscribe((res) => {
      this._t = { ...this._t, ...res }
    });

    this.credentials = this.fb.group({
      email: [undefined, [Validators.required, Validators.email]],
      password: [undefined, [Validators.required, Validators.minLength(8)]],
    });
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(false);
  }

  async doLogin() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.authService.login(this.credentials.value).subscribe(
      async (res) => {
        await loading.dismiss();
        this.router.navigateByUrl('/home', { replaceUrl: true });
      },
      async (err) => {
        await loading.dismiss();
        if (typeof err.error.error.detail === 'object')
          await this.toastController.dangerTitle(Object.values(err.error.error.detail).toString(), this._t.ERROR_LOGIN);
        else
          await this.toastController.dangerTitle(err.error.error.detail, this._t.ERROR_LOGIN);
      }
    );
  }

  // Easy access for form fields
  get email() {
    return this.credentials.get('email');
  }

  get password() {
    return this.credentials.get('password');
  }

}
