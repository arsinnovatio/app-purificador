import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment'
 
@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  version : string = '1.0.0';
  constructor() { }

  ngOnInit() {
    this.version = environment.version;
  }

}
