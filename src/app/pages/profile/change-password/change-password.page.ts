import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { MapPickerComponent } from "src/app/component/map-picker/map-picker.component";

import { UserService } from "src/app/services/user.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  _t: any = {};
  formData = {
    password: "",
    repeatPassword: "",
    new_password: ""
  }
  constructor(private _translate: TranslateService,
    private userService: UserService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) { }

  ngOnInit() {
    this._translate.get('ERRORS').subscribe((res: string) => {
      this._t = res;
      console.log(res);
    });
  }

  async doChangePassword() {

    const loading = await this.loadingController.create();
    await loading.present();

    this.userService.changePassword(this.formData).subscribe(async (res: any) => {
      await loading.dismiss();
      await this.toastController.success(this._t.SUCCESS_PASSWORD);
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      if (typeof err.error.error.detail === 'object')
        await this.toastController.dangerTitle(Object.values(err.error.error.detail).toString(), this._t.ERROR_PASSWORD);
      else
        await this.toastController.dangerTitle(err.error.error.detail, this._t.ERROR_PASSWORD);
    })

  }

}
