import { OnInit, Component, ViewChild, ElementRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Platform, ActionSheetController, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-change-pic',
  templateUrl: './change-pic.component.html',
  styleUrls: ['./change-pic.component.scss'],
})
export class ChangePicComponent implements OnInit {
  @ViewChild('fileInput', { static: false }) fileInput: ElementRef;

  constructor(public userService: UserService, 
    public modalController: PopoverController) { }

  ngOnInit() { }

  async goToCamera() {
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: true,
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera,
      width: 640,
      height: 640
    });

    const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);
    const imageName = 'Give me a name';

    this.userService.updatePic(blobData, imageName, image.format).subscribe(async (res: any) => {
      console.log(res.data.user)

      await this.modalController.dismiss({
        'dismissed': false,
        'user': res.data.user
      });
    });
  }

  async goToGallery() {
    const image = await Camera.getPhoto({
      quality: 60,
      allowEditing: true,
      resultType: CameraResultType.Base64,
      source: CameraSource.Photos
    });

    const blobData = this.b64toBlob(image.base64String, `image/${image.format}`);
    const imageName = 'Give me a name';

    this.userService.updatePic(blobData, imageName, image.format).subscribe((data) => {
      console.log(data)
    });
  }

  b64toBlob(b64Data, contentType = '', sliceSize = 512) {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

}
