import { Component, OnInit } from '@angular/core';
import { PopoverController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ChangePicComponent } from './change-pic/change-pic.component';
import { environment } from 'src/environments/environment';
import { Storage } from '@capacitor/storage';

import { UserService } from "src/app/services/user.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  _t: any = {};
  user: any = {};
  lang = 'en-EN';
  serverPath = environment.api_host;

  constructor(private _translate: TranslateService,
    public popoverController: PopoverController,
    public userService: UserService,
    public alertController: AlertController,
    public nav: NavController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController,) {

    this._translate.get('PROFILE').subscribe((res: string) => {
      this._t = res;
    });

  }

  async ngOnInit() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.userService.getUser().subscribe(async (res: any) => {
      this.user = res.data.user;
      this.lang = this._translate.currentLang;
      console.log(this.lang);
      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    })
  }

  async changePic(event) {
    const popover = await this.popoverController.create({
      component: ChangePicComponent,
      cssClass: 'help-popover',
      event: event,
      translucent: true,
    });
    await popover.present();

    const { role, data } = await popover.onDidDismiss();
    
    console.log('onDidDismiss resolved with role', role, data);
    console.log(data);
    if(role != 'backdrop'){
      this.user = data.user;
    }
  }

  async setName() {
    const alert = await this.alertController.create({
      header: this._t.NAME,
      message: this._t.CHANGENAME,
      cssClass: 'alertInput',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: this._t.NAME,
          value: this.user.name
        },
      ],
      buttons: [
        {
          text: this._t.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: this._t.CONFIRM,
          role: 'confirm',
          cssClass: 'primary',
        }
      ]
    });

    await alert.present();

    const { role, data } = await alert.onDidDismiss();

    console.log(data, role)
    if (role == 'confirm' && data) {
      this.user.name = data.values.name;
      this.updateUser();
    }
  }

  async setSurname() {
    const alert = await this.alertController.create({
      header: this._t.SURNAME,
      message: this._t.CHANGESURNAME,
      cssClass: 'alertInput',
      inputs: [
        {
          name: 'surname',
          type: 'text',
          placeholder: this._t.SURNAME,
          value: this.user.surname
        },
      ],
      buttons: [
        {
          text: this._t.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: this._t.CONFIRM,
          role: 'confirm',
          cssClass: 'primary',
        }
      ]
    });

    await alert.present();

    const { role, data } = await alert.onDidDismiss();

    console.log(data, role)
    if (role == 'confirm' && data) {
      this.user.surname = data.values.surname;
      this.updateUser();
    }
  }

  async changeLang($event){
    console.log($event.target.value);
    this._translate.use($event.target.value);
    Storage.set({key: "lang", value: $event.target.value});
  }

  async updateUser() {
    const loading = await this.loadingController.create();
    await loading.present();

    console.log(this.user);

    this.userService.updateUser(this.user).subscribe(async (res: any) => {
      this.user = res.data.user;
      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    })
  }

  goToChangePassword() {
    this.nav.navigateForward('profile/change-password', { state: { user: this.user } })
  }

  goToNotifications() {
    this.nav.navigateForward('profile/notifications', { state: { user: this.user } })
  }

  goToLocation() {
    this.nav.navigateForward('profile/location', { state: { user: this.user } })
  }

}
