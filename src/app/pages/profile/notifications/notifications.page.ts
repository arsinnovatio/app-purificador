
import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { MapPickerComponent } from "src/app/component/map-picker/map-picker.component";
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from "src/app/services/user.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
  user: any = {};
  _t: any = {};
  load = false;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private _translate: TranslateService,
    private userService: UserService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) {

    this._translate.get('ERRORS').subscribe((res: string) => {
      this._t = res;
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      let navParams = this.router.getCurrentNavigation().extras.state;
      console.log(navParams);
      this.user = navParams.user;

      setTimeout(() => {
        this.load = true;
      }, 500);

    });
  }

  async updateProfile() {
    if (this.load) {
      const loading = await this.loadingController.create();
      await loading.present();

      this.userService.updateNotifications(this.user).subscribe(async (res: any) => {
        await loading.dismiss();
        await this.toastController.success(this._t.SUCCESS_NOTIFICATIONS);
      }, async (err) => {
        console.log(err);
        await loading.dismiss();
        if (typeof err.error.error.detail === 'object')
          await this.toastController.dangerTitle(Object.values(err.error.error.detail).toString(), this._t.ERROR_PASSWORD);
        else
          await this.toastController.dangerTitle(err.error.error.detail, this._t.ERROR_PASSWORD);
      });
    }
  }

}
