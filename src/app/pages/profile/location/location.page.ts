import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router'
import { MapPickerComponent } from "src/app/component/map-picker/map-picker.component"
import { NewLocationComponent } from "./new-location/new-location.component";

import { UserService } from "src/app/services/user.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {
  user: any = {};
  locations: any = [];

  constructor(public modalController: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) { }

  async ngOnInit() {
    this.route.queryParams.subscribe(async (params) => {
      let navParams = this.router.getCurrentNavigation().extras.state;
      console.log(navParams);
      this.user = navParams.user;

      const loading = await this.loadingController.create();
      await loading.present();

      this.userService.getLocations().subscribe(async (res: any) => {
        this.user.locations = res.data.locations;
        await loading.dismiss();
      }, async (err) => {
        console.log(err);
        await loading.dismiss();
        await this.toastController.danger("ERROR");
      })
    });
  }

  async locationPicker() {
    const modal = await this.modalController.create({
      component: NewLocationComponent,
    });

    modal.onDidDismiss()
      .then((result) => {
        if (result.data != undefined) {
          console.log(result.data);
          if (this.user.locations && this.user.locations.length) {
            this.user.locations.push(result.data.location)
          } else {
            this.user.locations = [];
            this.user.locations.push(result.data.location)
          }
        }
      });

    return await modal.present();
  };

  async goToLocation(event, location) {
    const modal = await this.modalController.create({
      component: MapPickerComponent,
      componentProps: { lat: parseFloat(location.lat), lng: parseFloat(location.lng), allowSave: false }
    });

    return await modal.present();
  };

  async setLocationPrefered(event, location){
    const loading = await this.loadingController.create();
    await loading.present();

    this.userService.setLocation(location).subscribe(async (res: any) => {
      this.user.location = res.data.location_id;
      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    })
  }


  /**
   * Deletes location and actualize actual locations to server response
   * @param location 
   */
  async deleteLocation(event, location) {   
    const loading = await this.loadingController.create();
    await loading.present();

    this.userService.deleteLocation(location).subscribe(async (res: any) => {
      this.user.locations = res.data.locations;
      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    })
  }

}