import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { MapPickerComponent } from "src/app/component/map-picker/map-picker.component";

import { UserService } from "src/app/services/user.service";
import { ToastServiceService } from "src/app/services/toast-service.service";

@Component({
  selector: 'app-new-location',
  templateUrl: './new-location.component.html',
  styleUrls: ['./new-location.component.scss'],
})
export class NewLocationComponent implements OnInit {
  _t: any = {};
  location: any = {
    name: "",
    lat : 38.3844657,
    lng : -0.5144585
  }

  constructor(private _translate: TranslateService,
    public modalController: ModalController,
    public alertController: AlertController,
    public userService: UserService,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) {

    this._translate.get('PROFILE').subscribe((res: string) => {
      this._t = res;
    });

  }

  ngOnInit() { }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true,
    });
  }

  async send() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.userService.newLocation(this.location).subscribe(async (res: any) => {
      this.location = res.data.location;
      await loading.dismiss();  

      this.modalController.dismiss({
        'dismissed': false,
        'location': this.location
      });
      
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    })
  }

  async setName() {
    const alert = await this.alertController.create({
      header: this._t.FULLNAME,
      message: this._t.CHANGENAME,
      cssClass: 'alertInput',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: this._t.NAME,
          value: this.location.name
        },
      ],
      buttons: [
        {
          text: this._t.CANCEL,
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: this._t.CONFIRM,
          role: 'confirm',
          cssClass: 'primary',
        }
      ]
    });

    await alert.present();

    const { role, data } = await alert.onDidDismiss();

    console.log(data, role)
    if (role == 'confirm' && data) {
      this.location.name = data.values.name;
    }
  }

  async locationPicker() {
    console.log("HEY")
    const modal = await this.modalController.create({
      component: MapPickerComponent,
      componentProps: {lat: this.location.lat, lng: this.location.lng, allowSave: true}
    });

    modal.onDidDismiss()
      .then((result) => {
        if (result.data != undefined) {
          console.log(result.data);
          this.location.lat = result.data.lat;
          this.location.lng = result.data.lng;
        }
      });

    return await modal.present();
  };
}
