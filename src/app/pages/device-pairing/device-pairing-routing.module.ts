import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DevicePairingPage } from './device-pairing.page';

const routes: Routes = [
  {
    path: '',
    component: DevicePairingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevicePairingPageRoutingModule {}
