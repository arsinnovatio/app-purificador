import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, from, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements HttpInterceptor {
  _token = '';

  constructor(
    private router: Router
  ) {
    this.init();
  }

  async init() {
    const token = await Storage.get({ key: 'auth_token' });
    this._token = token.value;

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return from(this.handle(req, next)).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401) {
          this.router.navigateByUrl('/login');
        }
        return throwError(err);
      })
    );
  }

  async handle(req: HttpRequest<any>, next: HttpHandler) {
    const token = await Storage.get({ key: 'auth_token' })
    this._token = token.value;

    let request = req;
    if (this._token) {
      const token: string = this._token;

      if (token && token != "") {
        request = req.clone({
          setHeaders: {
            'Authorization': `Bearer ${token}`
          }
        });
      }
    }

    return next.handle(request).toPromise()
  }
}
