import { Component, OnInit, Input} from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
//import { MouseEvent } from '@agm/core';
import { HttpClient } from '@angular/common/http';
import { google } from '@google/maps';


declare var google;
@Component({
  selector: 'app-map-picker',
  templateUrl: './map-picker.component.html',
  styleUrls: ['./map-picker.component.scss'],
})
export class MapPickerComponent implements OnInit {

  static apiKey: string = 'AIzaSyB0MK4psEeCaUZnvHbGbeoTh9ghyGf1LPY';
  
  apiKey: string = MapPickerComponent.apiKey;
  query: string = '';
  places: any = [];
  @Input() lat;
  @Input() lng;
  @Input() allowSave;

  map = {
    lat : 38.3844657,
    lng : -0.5144585
  }
  mark = {
    lat : 38.3844657,
    lng : -0.5144585
  }

  constructor(public modalCtrl: ModalController, public http: HttpClient, public navParams: NavParams) { }

  ngOnInit() {
    console.log(this.navParams);
    this.map = {
      lat: this.lat,
      lng: this.lng,
    };
    this.mark = {
      lat: this.lat,
      lng: this.lng,
    };
  }
  
  markerDragEnd(event: google.maps.MouseEvent) {
    //console.log('dragEnd', m, event)
    console.log(event)
    this.mark.lat = event.latLng.lat()
    this.mark.lng = event.latLng.lng()
  };

  save() {
    this.modalCtrl.dismiss(this.mark)
  };

  close() {
    this.modalCtrl.dismiss()
  };
  
  searchPlace($event: any) {
    console.log($event.target.value)
    this.query = $event.target.value

    const config = {
      // types: ['geocode'],
      input: this.query
    }

    if (this.query.length > 3) {
      // this.mapsAPILoader.load().then(() => {
        let autocompleteService = new google.maps.places.AutocompleteService()

        autocompleteService.getPlacePredictions(config, (predictions, status) => {
          if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {
            console.log(predictions)
            
            this.places = predictions;
          }
        })
      // })
    } else {
      this.places = [];
    }
  };

  selectPlace(place: any){
    console.log(place)
    
    this.places = [];
    this.query = ""

    this.http.get('https://maps.googleapis.com/maps/api/geocode/json?place_id='+place.place_id+'&key='+this.apiKey+'')
    .subscribe(
      (response: Response) => {

        // this.posts = data;
        const data: any = response
        console.log(data);
        console.log(data.results[0].geometry.location)

        this.map.lat = data.results[0].geometry.location.lat;
        this.map.lng = data.results[0].geometry.location.lng;

        this.mark.lat = data.results[0].geometry.location.lat;
        this.mark.lng = data.results[0].geometry.location.lng;
        // return data;
      },
      (error) => console.log({
        status: 'Gagal Terhubung',
        error
      })
    )
  };

}