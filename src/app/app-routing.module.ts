import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IntroGuard } from './guards/intro.guard';
import { AutoLoginGuard } from './guards/auto-login.guard';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canLoad: [IntroGuard, AutoLoginGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'recover',
    loadChildren: () => import('./pages/recover/recover.module').then( m => m.RecoverPageModule)
  },
  {
    path: 'recover/:token',
    loadChildren: () => import('./pages/recover/recover.module').then( m => m.RecoverPageModule)
  },
  {
    path: 'device-add',
    loadChildren: () => import('./pages/device-add/device-add.module').then( m => m.DeviceAddPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device-add/device/:id',
    loadChildren: () => import('./pages/device-add/connect/connect.module').then( m => m.ConnectPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device-pairing',
    loadChildren: () => import('./pages/device-pairing/device-pairing.module').then( m => m.DevicePairingPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device-pairing-help',
    loadChildren: () => import('./pages/device-pairing-help/device-pairing-help.module').then( m => m.DevicePairingHelpPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device/:id',
    loadChildren: () => import('./pages/device/device.module').then( m => m.DevicePageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device-modes',
    loadChildren: () => import('./pages/device-modes/device-modes.module').then( m => m.DeviceModesPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device/:id/data',
    loadChildren: () => import('./pages/device-data/device-data.module').then( m => m.DeviceDataPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device/:id/settings',
    loadChildren: () => import('./pages/device-settings/device-settings.module').then( m => m.DeviceSettingsPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'device/:id/help',
    loadChildren: () => import('./pages/device-help/device-help.module').then( m => m.DeviceHelpPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'messages',
    loadChildren: () => import('./pages/messages/messages.module').then( m => m.MessagesPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'help',
    loadChildren: () => import('./pages/help/help.module').then( m => m.HelpPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'logout',
    loadChildren: () => import('./pages/logout/logout.module').then( m => m.LogoutPageModule),
    canLoad: [AuthGuard] 
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/help/tutorial/tutorial.module').then( m => m.TutorialPageModule)
  },
  {
    path: 'legal',
    loadChildren: () => import('./pages/legal/legal.module').then( m => m.LegalPageModule)
  },
  {
    path: 'service',
    loadChildren: () => import('./pages/service/service.module').then( m => m.ServicePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
