import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  _t: any = {};
  _token = '';

  constructor(private _translate: TranslateService, private http: HttpClient) {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });
  }

  getFeeds() {
    //http://localhost/api/v1/feedback
    return this.http.get(`${environment.api_host}/api/v1/feedback`, {
    }).pipe();
  }

  sendFeedback(deviceId, feedback) {
    //http://localhost/api/v1/feedback
    //TODO feedback linked with device
    return this.http.post(`${environment.api_host}/api/v1/feedback`, feedback, {
    }).pipe();
  }

  deleteFeedback(feedback) {
    //http://localhost/api/v1/feedback/id
    return this.http.delete(`${environment.api_host}/api/v1/feedback/${feedback.id}`, {}).pipe();
  }
}
