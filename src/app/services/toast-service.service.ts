import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class ToastServiceService {
  _t: any = {};

  constructor(private toastController: ToastController,
    private _translate: TranslateService) {

    this._translate.get('SETTINGS').subscribe((res: string) => {
      this._t = res;
    });

  }

  async success(message) {
    var toast = await this.toastController.create({
      message: message,
      color: 'success',
      position: 'top',
      duration: 3000,
      buttons: [{
        text: this._t.CLOSE,
        role: 'cancel',
      }]
    });
    toast.present();
  }

  async danger(message) {
    var toast = await this.toastController.create({
      message: message,
      color: 'danger',
      position: 'top',
      duration: 3000,
      buttons: [{
        text: this._t.CLOSE,
        role: 'cancel',
      }]
    });
    toast.present();
  }

  async dangerTitle(message, title) {
    var toast = await this.toastController.create({
      header: title,
      message: message,
      color: 'danger',
      position: 'top',
      duration: 3000,
      buttons: [{
        text: this._t.CLOSE,
        role: 'cancel',
      }]
    });
    toast.present();
  }
}
