import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';
import { AlertController, LoadingController } from '@ionic/angular';

import { ToastServiceService } from "src/app/services/toast-service.service";


@Injectable({
  providedIn: 'root'
})
export class DeviceDataService {
  _token = '';
  _t: any = {};

  fakeAlerts = [
    {
      title: 'Detected a incorrect values',
      text: 'The temperature value is outside the values determined for the comfort zone. The recorded value is 25 ºC and it is recommended not to exceed 24ºC.',
      date: '2 days ago. (12:20)',
      type: 'alert'
    },
    {
      title: 'Detected a incorrect values',
      text: 'The temperature value is outside the values determined for the comfort zone. The recorded value is 25 ºC and it is recommended not to exceed 24ºC.',
      date: '2 days ago. (12:20)',
      type: 'alert'
    },
    {
      title: 'Detected a dangerous value',
      text: 'The co2 value has reached a level where continued exposure may have adverse effects. The recorded value is 1200ppm while the maximum recommended value is 800ppm.',
      date: '2 days ago. (12:20)',
      type: 'danger'
    },
    {
      title: 'Detected a incorrect values',
      text: 'The temperature value is outside the values determined for the comfort zone. The recorded value is 25 ºC and it is recommended not to exceed 24ºC.',
      date: '2 days ago. (12:20)',
      type: 'alert'
    }
  ]

  constructor(private _translate: TranslateService,
    private http: HttpClient,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });

    this.init();
  }

  async init() {
    const token = await Storage.get({ key: 'auth_token' });
    this._token = token.value;
  }

  getAlerts(){
    return this.fakeAlerts;
  }

  getData(deviceId, variable, start, end) {
    return this.http.get(`${environment.api_host}/api/v1/devices/${deviceId}/data/historic?variable=${variable}&limit=1440&start=${start}&end=${end}`, {
    }).pipe();
  }
}
