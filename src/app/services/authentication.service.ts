import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from "src/app/interceptors/auth.service";

import { Storage } from '@capacitor/storage';

const TOKEN_KEY = 'auth_token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // Init with null to filter out the first value in a guard!
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  token = '';

  constructor(private http: HttpClient, private authInterceptor: AuthService) {
    this.loadToken();
  }

  async loadToken() {
    const token = await Storage.get({ key: TOKEN_KEY });
    if (token && token.value && token.value !== undefined) {
      this.token = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  login(credentials: { email, password }): Observable<any> {
    return this.http.post(`${environment.api_host}/api/login`, credentials).pipe(
      map((res: any) => {
        Storage.set({ key: TOKEN_KEY, value: res.data.access_token });
        this.token = res.data.access_token;
        this.authInterceptor._token = res.data.access_token;
        return res
      }),
      tap(_ => {
        this.isAuthenticated.next(true);
      })
    )
  }

  logout(): Promise<void> {
    this.isAuthenticated.next(false);
    return Storage.remove({ key: TOKEN_KEY });
  }

  register(credentials: { email, password }): Observable<any> {
    return this.http.post(`${environment.api_host}/api/register`, credentials).pipe();
  }
}
