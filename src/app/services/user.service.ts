import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  _t: any = {};

  constructor(private _translate: TranslateService, private http: HttpClient) {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });
  }

  getUser() {
    //http://localhost/api/v1/profile
    return this.http.get(`${environment.api_host}/api/v1/profile`, {
    }).pipe();
  }

  updateUser(user) {
    //http://localhost/api/v1/profile
    return this.http.post(`${environment.api_host}/api/v1/profile`, {name: user.name, surname: user.surname}, {
    }).pipe();
  }

  updateNotifications(user) {
    //http://localhost/api/v1/profile
    return this.http.post(`${environment.api_host}/api/v1/profile/notifactions`, {promo_notifications: user.promo_notifications, firmware_notifications: user.firmware_notifications, device_notifications: user.device_notifications}, {
    }).pipe();
  }

  changePassword(user) {
    //http://localhost/api/v1/profile
    return this.http.post(`${environment.api_host}/api/v1/profile/password`, {password: user.password, new_password: user.new_password}, {
    }).pipe();
  }

  getLocations() {
    //http://localhost/api/v1/profile/locations
    return this.http.get(`${environment.api_host}/api/v1/profile/locations`, {
    }).pipe();
  }

  setLocation(location) {
    //http://localhost/api/v1/profile/locations/:id
    return this.http.get(`${environment.api_host}/api/v1/profile/locations/${location.id}`, {
    }).pipe();
  }

  newLocation(location) {
    //http://localhost/api/v1/profile/locations
    //TODO feedback linked with device
    return this.http.post(`${environment.api_host}/api/v1/profile/locations`, location, {
    }).pipe();
  }

  deleteLocation(location) {
    //http://localhost/api/v1/profile/locations/id
    return this.http.delete(`${environment.api_host}/api/v1/profile/locations/${location.id}`, {}).pipe();
  }

  updatePic(blobData, name, ext){
    const formData = new FormData();
    formData.append('image', blobData, `myimage.${ext}`);
    formData.append('name', name);

    return this.http.post(`${environment.api_host}/api/v1/profile/photo`, formData, {
    }).pipe();
  }
}
