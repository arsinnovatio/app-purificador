import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';
import { AlertController, LoadingController } from '@ionic/angular';

import { ToastServiceService } from "src/app/services/toast-service.service";

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  _t: any = {};
  _token = '';
  device: any = {
    id: 12,
    name: "Air purifier",
    mode: 1,
    mode_icon: "moon",
    connected: true,
    last_data: {
      pm25: 23,
      pm10: 13,
      temp: 15,
      humidity: 61,
      co2: 3450,
      co: 3,
      hvoc: 13,
      aqi: 27,
      date: new Date().getTime(),
    }
  }

  constructor(private _translate: TranslateService,
    private http: HttpClient,
    public alertController: AlertController,
    public toastController: ToastServiceService,
    public loadingController: LoadingController) {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });

    this.init();
  }

  async init() {
    const token = await Storage.get({ key: 'auth_token' });
    this._token = token.value;
  }

  getDevices() {
    //http://localhost/api/devices/
    return this.http.get(`${environment.api_host}/api/v1/devices`, {
    }).pipe();
  }

  getDevice(id) {
    return this.http.get(`${environment.api_host}/api/v1/devices/${id}`, {
    }).pipe();
  }

  changeMode(device, mode) {
    return this.http.get(`${environment.api_host}/api/v1/devices/${device.id}/mode/${mode}`, {}).pipe();
  }

  getActualMode(device) {
    return this.getModeObject(device.settings.mode);
  }

  getModesArray(modeToRemove?) {
    var modes = [
      {
        id: 0,
        icon: "radio-button-off",
        title: this._t.OFF,
        text: this._t.OFF_TEXT
      },
      {
        id: 1,
        icon: "cellular",
        title: this._t.AUTO,
        text: this._t.AUTO_TEXT
      },
      {
        id: 2,
        icon: "flash",
        title: this._t.MAX,
        text: this._t.MAX_TEXT
      },
      {
        id: 4,
        icon: "leaf",
        title: this._t.ECO,
        text: this._t.ECO_TEXT
      },
      {
        id: 5,
        icon: "moon",
        title: this._t.NIGHT,
        text: this._t.NIGHT_TEXT
      },
    ]

    if (modeToRemove) {
      let index = modes.findIndex((item) => {
        return item.icon == modeToRemove.icon
      })
      modes.splice(index, 1)
    }

    return modes
  }

  getModeObject(mode) {
    var modes = this.getModesArray();
    var modeObj = modes.find((item) => {
      return item.id == mode
    });
    return modeObj;
  }

  getSettings(device) {
    return device.settings;
  }

  async setSettings(device, setting: string, value: any) {
    const loading = await this.loadingController.create();
    await loading.present();

    device.settings[setting] = value;

    this.updateSettings(device.id, device.settings).subscribe(async (res: any) => {
      console.log(res);
      device = res.data.device;
      await loading.dismiss();
    }, async (err) => {
      console.log(err);
      await loading.dismiss();
      await this.toastController.danger("ERROR");
    });
  }

  updateSettings(deviceId, settings) {
    return this.http.post(`${environment.api_host}/api/v1/devices/${deviceId}/settings`, settings, {
    }).pipe();
  }

  unlinkDevice(deviceId) {
    //http://localhost/api/v1/feedback/id
    return this.http.delete(`${environment.api_host}/api/v1/devices/${deviceId}`, {}).pipe();
  }

  sendFeedback(deviceId, feedback) {
    //TODO feedback linked with device
  }

  transformMeasureToCualification(device, measure) {
    if (device.last_data) {
      switch (measure) {
        case 'pm25':
          return this.transformPM25(device);
        case 'pm10':
          return this.transformPM10(device);
        case 'aqi':
          return this.transformAQI(device);
        case 'hvoc':
          return this.transformHVOC(device);
        case 'co':
          return this.transformCO(device);
        case 'co2':
          return this.transformCO2(device);
        default:
          return 'GOOD';
      }
    }
    return 'GOOD';
  }

  transformPM25(device) {
    if (device.last_data.pm25) {
      let v = device.last_data.pm25;
      if (v <= 50) {
        return 'GOOD';
      }
      if (v <= 150) {
        return 'OKAY';
      }
      return 'BAD';
    }
    return 'GOOD';
  }

  transformPM10(device) {
    if (device.last_data.pm10) {
      let v = device.last_data.pm10;
      if (v <= 50) {
        return 'GOOD';
      }
      if (v <= 150) {
        return 'OKAY';
      }
      return 'BAD';
    }
    return 'GOOD';
  }

  transformAQI(device) {
    if (device.last_data.aqi) {
      let v = device.last_data.aqi;
      if (v <= 50) {
        return 'GOOD';
      }
      if (v <= 150) {
        return 'OKAY';
      }
      return 'BAD';
    }
    return 'GOOD';
  }

  transformHVOC(device) {
    if (device.last_data.hcoh) {
      let v = device.last_data.hcoh;
      if (v <= 50) {
        return 'GOOD';
      }
      if (v <= 150) {
        return 'OKAY';
      }
      return 'BAD';
    }
    return 'GOOD';
  }

  transformCO(device) {
    if (device.last_data.co) {
      let v = device.last_data.co;
      if (v <= 10) {
        return 'GOOD';
      }
      if (v <= 20) {
        return 'OKAY';
      }
      return 'BAD';
    }
    return 'GOOD';
  }

  transformCO2(device) {
    if (device.last_data.co2) {
      let v = device.last_data.co2;
      if (v <= 800) {
        return 'GOOD';
      }
      if (v <= 1200) {
        return 'OKAY';
      }
      return 'BAD';
    }
    return 'GOOD';
  }

  isDirt(device) {
    if (this.transformPM25(device) != 'GOOD') return true;
    console.log(this.transformPM25(device));
    if (this.transformPM10(device) != 'GOOD') return true;
    console.log(this.transformPM10(device));
    if (this.transformAQI(device) != 'GOOD') return true;
    console.log(this.transformAQI(device));
    if (this.transformHVOC(device) != 'GOOD') return true;
    console.log(this.transformHVOC(device));
    if (this.transformCO(device) != 'GOOD') return true;
    console.log(this.transformCO(device));
    if (this.transformCO2(device) != 'GOOD') return true;
    console.log(this.transformCO2(device));
    return false
  }
}
