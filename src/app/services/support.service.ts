import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class SupportService {
  _t: any = {};
  _token = '';

  constructor(private _translate: TranslateService, private http: HttpClient) {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });
  }

  getTickets() {
    //http://localhost/api/v1/support-request
    return this.http.get(`${environment.api_host}/api/v1/support-request`, {}).pipe();
  }

  sendFeedback(ticket) {
    //http://localhost/api/v1/support-request
    return this.http.post(`${environment.api_host}/api/v1/support-request`, ticket, {}).pipe();
  }

  updateTicket(ticket, response) {
    console.log(response);
    //http://localhost/api/v1/support-request/id
    return this.http.post(`${environment.api_host}/api/v1/support-request/${ticket.id}/add-response`, response, {}).pipe();
  }

  deleteTicket(ticket) {
    //http://localhost/api/v1/support-request/id
    return this.http.delete(`${environment.api_host}/api/v1/support-request/${ticket.id}`, {}).pipe();
  }

}
