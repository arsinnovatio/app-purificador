import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, switchMap } from 'rxjs/operators';
import { BehaviorSubject, from, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class DeviceConnectService {
  _t: any = {};
  _token = '';

  device: any = {};

  constructor(private _translate: TranslateService, private http: HttpClient) {
    this._translate.get('DEVICE.MODES').subscribe((res: string) => {
      this._t = res;
    });

    this.init();
  }

  async init() {
    const token = await Storage.get({ key: 'auth_token' });
    this._token = token.value;
  }


  checkConnection(device_id) {
    //http://localhost/api/v1/devices/
    return this.http.get(`${environment.api_host}/api/v1/devices/check/${device_id}`, {
    }).pipe();
  }

}
