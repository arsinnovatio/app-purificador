import { TestBed } from '@angular/core/testing';

import { DeviceConnectService } from './device-connect.service';

describe('DeviceConnectService', () => {
  let service: DeviceConnectService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeviceConnectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
