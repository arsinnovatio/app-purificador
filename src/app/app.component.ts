import { Component } from '@angular/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@capacitor/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public language: string;
  public appPages = [
    { title: 'Home', url: '/home', icon: 'mail' },
    { title: 'Profile', url: '/profile', icon: 'person' },
    { title: 'Help', url: '/help', icon: 'paper-plane' },
    { title: 'About', url: '/about', icon: 'heart' },
    { title: 'Logout', url: '/logout', icon: 'archive' },
  ];

  constructor(private globalization: Globalization, private _translate: TranslateService) {
    this.getDeviceLanguage();
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang('en-EN');
    if (language) {
      this.language = language;
    }
    else {
      // Set your language here
      this.language = 'en-EN';
    }
    this._translateLanguage();
  }

  async getDeviceLanguage() {
    const lang = await Storage.get({ key: 'lang' })
    console.log(lang);
    if (lang && lang.value && lang.value !== undefined) {
      console.log("HAY LANG", lang);
      this._initTranslate(lang.value);
    } else {
      if (window.Intl && typeof window.Intl === 'object') {
        this._initTranslate(navigator.language)
      }
      else {
        this.globalization.getPreferredLanguage()
          .then(res => {
            this._initTranslate(res.value)
          })
          .catch(e => { console.log(e); });
      }
    }
  }

}
